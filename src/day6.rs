use std::collections::HashMap;

static INPUT: &'static str = include_str!("../inputs/day6.txt");

// I've been using iterators a bunch, but this one
// seems pretty clean as imperative
fn solve(input: &str) -> (String, String) {
    let line_len = input.split_whitespace().next().unwrap().len();
    let mut freqs = vec![HashMap::new(); line_len];
    
    // Count letters in each column
    for line in input.split_whitespace() {
        for (char, table) in line.chars().zip(freqs.iter_mut()) {
            *table.entry(char).or_insert(0) += 1;
        }
    }
    
    // Collect most and least common letters in each map
    let maxes = freqs.iter().filter_map(|table| {
        table.iter().max_by_key(|&(_, &count)| count).map(|(&ch, _)| ch)
    }).collect::<String>();
    let mins = freqs.iter().filter_map(|table| {
        table.iter().min_by_key(|&(_, &count)| count).map(|(&ch, _)| ch)
    }).collect::<String>();
    
    (maxes, mins)
}

fn main() {
    let (first, second) = solve(INPUT);
    println!("First: {}", first);
    println!("Second: {}", second);
}

#[cfg(test)]
mod tests {
    use super::solve;
    
    static TEST_INPUT: &'static str = r#"eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar"#;
    
    #[test]
    fn part1_example() {
        let (first, _) = solve(TEST_INPUT);
        assert_eq!(first, "easter");
    }
    
    #[test]
    fn part2_example() {
        let (_, second) = solve(TEST_INPUT);
        assert_eq!(&second[..2], "ad");
    }
}