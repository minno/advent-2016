#[macro_use]
extern crate nom;

use nom::{alpha, space};
use std::collections::HashMap;

static INPUT: &'static str = include_str!("../inputs/day12.txt");

named!(parse_input<&str, Vec<Opcode> >,
    separated_list!(tag_s!("\n"), parse_opcode)
);
named!(parse_opcode<&str, Opcode>,
    do_parse!(
        op: alpha
        >> space
        >> val1: parse_value
        >> opt!(space)
        >> val2: opt!(parse_value)
        >> (to_opcode(op, val1, val2))
    )
);
fn to_opcode(op: &str, val1: Value, val2: Option<Value>) -> Opcode {
    fn to_register(val: Value) -> Register {
        match val {
            Value::Register(reg) => reg,
            _ => panic!("Called to_register on a literal")
        }
    }
    fn to_literal(val: Value) -> i32 {
        match val {
            Value::Literal(val) => val,
            _ => panic!("Called to_literal on a register")
        }
    }
    
    match op {
        "cpy" => Opcode::Cpy(val1, to_register(val2.unwrap())),
        "inc" => Opcode::Inc(to_register(val1)),
        "dec" => Opcode::Dec(to_register(val1)),
        "jnz" => Opcode::Jnz(val1, to_literal(val2.unwrap())),
        _ => panic!("Unexpected opcode: {:?}", op)
    }
}
named!(parse_value<&str, Value>,
    map!(
        take_while_s!(is_value_char),
        |s: &str| match s {
            "a" => Value::Register(Register::A),
            "b" => Value::Register(Register::B),
            "c" => Value::Register(Register::C),
            "d" => Value::Register(Register::D),
            _ => Value::Literal(s.parse::<i32>().unwrap_or(0))
        }
    )
);
fn is_value_char(ch: char) -> bool {
    ch.is_alphanumeric() || ch == '-'
}

#[derive(Copy, Clone, Hash, PartialEq, Eq, Debug)]
enum Register {
    A,
    B,
    C,
    D
}

#[derive(Copy, Clone, Debug)]
enum Value {
    Register(Register),
    Literal(i32)
}

#[derive(Copy, Clone, Debug)]
enum Opcode {
    Cpy(Value, Register),
    Inc(Register),
    Dec(Register),
    Jnz(Value, i32)
}

#[derive(Clone, Debug)]
struct Computer {
    ip: i32,
    opcodes: Vec<Opcode>,
    registers: HashMap<Register, i32>
}

impl Computer {
    fn new(opcodes: Vec<Opcode>) -> Computer {
        Computer {
            ip: 0,
            opcodes: opcodes,
            registers: HashMap::new()
        }
    }
    
    fn apply(&mut self, op: Opcode) {
        match op {
            Opcode::Cpy(src, dst) => {
                let value = self.eval(src);
                self.set_reg(dst, value);
            },
            Opcode::Inc(reg) => *self.get_reg_mut(reg) += 1,
            Opcode::Dec(reg) => *self.get_reg_mut(reg) -= 1,
            Opcode::Jnz(value, offset) => {
                if self.eval(value) != 0 {
                    // Subtract one to counteract following increment
                    self.ip += offset - 1;
                }
            }
        }
    }
    
    // Returns true if execution may continue
    fn step(&mut self) -> bool {
        let op = self.opcodes[self.ip as usize];
        self.apply(op);
        self.ip += 1;
        if self.ip < 0 {
            self.ip = 0;
        }
        (self.ip as usize) < self.opcodes.len()
    }
    
    fn run(&mut self) {
        while self.step() {}
    }
    
    fn eval(&self, val: Value) -> i32 {
        match val {
            Value::Register(reg) => self.get_reg(reg),
            Value::Literal(val) => val
        }
    }
    
    fn get_reg(&self, reg: Register) -> i32 {
        *self.registers.get(&reg).unwrap_or(&0)
    }
    
    fn set_reg(&mut self, reg: Register, value: i32) {
        *self.get_reg_mut(reg) = value;
    }
    
    fn get_reg_mut(&mut self, reg: Register) -> &mut i32 {
        self.registers.entry(reg).or_insert(0)
    }
}

fn main() {
    let code = parse_input(INPUT).to_result().unwrap();
    let mut computer = Computer::new(code.clone());
    computer.run();
    println!("First: {}", computer.get_reg(Register::A));
    
    computer = Computer::new(code);
    computer.set_reg(Register::C, 1);
    computer.run();
    println!("Second: {}", computer.get_reg(Register::A));
}

#[cfg(test)]
mod test {
    use super::{Computer, Register, parse_input};
    
    #[test]
    fn part1_example() {
        let input = r#"cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a"#;
        println!("{:?}", parse_input(input));
        let mut comp = Computer::new(parse_input(input).to_result().unwrap());
        comp.run();
        assert_eq!(comp.get_reg(Register::A), 42);
    }
}