#[macro_use]
extern crate nom;

use std::collections::HashMap;

static INPUT: &'static str = include_str!("../inputs/day4.txt");

#[derive(Debug)]
struct Room<'a> {
    name: &'a str,
    id: i32,
    checksum: &'a str
}

impl<'a> Room<'a> {
    fn is_valid(&self) -> bool {
        let mut counts = HashMap::new();
        for ch in self.name.chars() {
            if ch == '-' { continue; }
            *counts.entry(ch).or_insert(0) += 1;
        }
        let mut sorted_counts = counts.into_iter().collect::<Vec<_>>();
        sorted_counts.sort_by_key(|&(ch, count)| (-count, ch));
        sorted_counts.into_iter().map(|(ch, _)| ch).take(5).eq(self.checksum.chars())
    }
    
    fn decrypt(&self) -> String {
        let offset = (self.id % 26) as u8;
        self.name.chars().map(|ch| match ch {
            '-' => ' ',
            _ => {
                let base = ch as u8 - 'a' as u8;
                ((base + offset) % 26 + 'a' as u8) as char
            }
        }).collect::<String>()
    }
}

// Nom parsers
fn is_name_char(ch: char) -> bool {
    ch.is_alphabetic() || ch == '-'
}
named!(parse_room_name<&str, &str>,
    take_while_s!(is_name_char)
);
named!(parse_room_id<&str, i32>,
    map!(take_while_s!(char::is_numeric), |s: &str| s.parse::<i32>().unwrap())
);
named!(parse_room_checksum<&str, &str>,
    do_parse!(
        tag_s!("[")                             >>
        chk: take_while_s!(char::is_alphabetic) >>
        tag_s!("]")                             >>
        (chk)
    )
);
named!(parse_room<&str, Room>,
    do_parse!(
        name: parse_room_name         >>
        id: parse_room_id             >>
        checksum: parse_room_checksum >>
        (Room { name: name.trim_right_matches('-'), id: id, checksum: checksum })
    )
);

// Iterator over inputs
struct Rooms<'a> {
    inner: std::str::Lines<'a>
}

impl<'a> Rooms<'a> {
    fn new(input: &'a str) -> Self {
        Rooms { inner: input.lines() }
    }
}

impl<'a> Iterator for Rooms<'a> {
    type Item = Room<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().and_then(|line| {
            parse_room(line).to_result().ok()
        })
    }
}

fn main() {
    let first = Rooms::new(INPUT)
        .filter(Room::is_valid)
        .map(|room| room.id)
        .sum::<i32>();
    let second = Rooms::new(INPUT)
        .filter(|room| room.decrypt().contains("north"))
        .next().unwrap().id;
    println!("First part: {}", first);
    println!("Second part: {}", second);
}