mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

fn main() {
    println!("Running all tests");
    println!("Day 1:");
    println!("{}", day1::solve());
    println!("Day 2:");
    println!("{}", day2::solve());
    println!("Day 3:");
    println!("{}", day3::solve());
    println!("Day 4:");
    println!("{}", day4::solve());
    println!("Day 5:");
    println!("{}", day5::solve());
    println!("Day 6:");
    println!("{}", day6::solve());
    println!("Day 7:");
    println!("{}", day7::solve());
    println!("Day 8:");
    println!("{}", day8::solve());
    println!("Day 9:");
    println!("{}", day9::solve());
    println!("Day 10:");
    println!("{}", day10::solve());
    println!("Day 11:");
    println!("{}", day11::solve());
    println!("Day 12:");
    println!("{}", day12::solve());
    println!("Day 13:");
    println!("{}", day13::solve());
    println!("Day 14:");
    println!("{}", day14::solve());
    println!("Day 15:");
    println!("{}", day15::solve());
    println!("Day 16:");
    println!("{}", day16::solve());
    println!("Day 17:");
    println!("{}", day17::solve());
    println!("Day 18:");
    println!("{}", day18::solve());
    println!("Day 19:");
    println!("{}", day19::solve());
    println!("Day 20:");
    println!("{}", day20::solve());
    println!("Day 21:");
    println!("{}", day21::solve());
    println!("Day 22:");
    println!("{}", day22::solve());
    println!("Day 23:");
    println!("{}", day23::solve());
    println!("Day 24:");
    println!("{}", day24::solve());
    println!("Day 25:");
    println!("{}", day25::solve());
}
