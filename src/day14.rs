// must...resist...nom

extern crate md5;

use std::io::Write;

static INPUT: &'static str = include_str!("../inputs/day14.txt");

struct HashCollection {
    buffer: Vec<u8>,
    seed_len: usize,
    is_stretched: bool,
    memo: Vec<[u8; 16]>
}

impl HashCollection {
    fn new(data: &str) -> Self {
        let mut buf = Vec::new();
        buf.extend(data.as_bytes());
        HashCollection { 
            buffer: buf, 
            seed_len: data.len(), 
            is_stretched: false,
            memo: Vec::new()
        }
    }
    
    fn stretch(mut self) -> Self {
        self.is_stretched = true;
        self
    }
    
    fn at(&mut self, idx: usize) -> [u8; 16] {
        for i in self.memo.len()..idx+1 {
            self.buffer.truncate(self.seed_len);
            write!(&mut self.buffer, "{}", i).unwrap();
            let result = if !self.is_stretched {
                md5::compute(&self.buffer).0
            } else {
                fn write_hex (slice: [u8; 16], buf: &mut [u8; 32]) {
                    for (idx, &byte) in slice.iter().enumerate() {
                        write!(&mut buf[idx*2..], "{:02x}", byte).unwrap();
                    }
                }
                
                let mut buffer = [0u8; 32];
                write_hex(md5::compute(&self.buffer).0, &mut buffer);
                for _ in 0..2015 {
                    let hash = md5::compute(&buffer[..]).0;
                    write_hex(hash, &mut buffer);
                }
                md5::compute(&buffer[..]).0
            };
            self.memo.push(result);
        }
        self.memo[idx]
    }
}

fn has_three(arr: [u8; 16]) -> Option<u8> {
    for idx in 0..16 {
        let byte = arr[idx];
        if (byte ^ (byte >> 4)) & 0x0f == 0 {
            if (idx > 0 && arr[idx-1] & 0x0f == byte & 0x0f) ||
               (idx < 15 && arr[idx+1] & 0xf0 == byte & 0xf0) {
                return Some(byte & 0x0f);
            }
        }
    }
    None
}

fn has_five(arr: [u8; 16], nib: u8) -> bool {
    for idx in 0..15 {
        let byte1 = arr[idx];
        let byte2 = arr[idx+1];
        if byte1 == ((nib << 4) | nib) && byte1 == byte2 {
            if (idx > 0 && arr[idx-1] & 0x0f == byte1 & 0x0f) ||
               (idx < 14 && arr[idx+2] & 0xf0 == byte1 & 0xf0) {
                return true;
            }
        }
    }
    false
}

fn has_following_five(hashes: &mut HashCollection, idx: usize, nib: u8) -> bool {
    for i in idx+1..idx+1001 {
        let hash = hashes.at(i);
        if has_five(hash, nib) {
            return true;
        }
    }
    return false;
}

fn get_keys(salt: &str, stretched: bool) -> Vec<(usize, [u8; 16])> {
    let mut hashes = HashCollection::new(salt);
    if stretched {
        hashes = hashes.stretch();
    }
    let mut keys = Vec::new();
    for idx in 0.. {
        let hash = hashes.at(idx);
        if let Some(nib) = has_three(hash) {
            if has_following_five(&mut hashes, idx, nib) {
                keys.push((idx, hash));
            }
        }
        if keys.len() >= 64 {
            break;
        }
    }
    keys
}

fn main() {
    println!("First: {}", get_keys(INPUT, false)[63].0);
    println!("Second: {}", get_keys(INPUT, true)[63].0);
}

#[cfg(test)]
mod test {
    use super::{get_keys};
    
    #[test]
    fn part1_example() {
        let res = get_keys("abc", false);
        assert_eq!(res.last().unwrap().0, 22728);
    }
    
    #[test]
    fn part2_example() {
        let res = get_keys("abc", true);
        assert_eq!(res[0].0, 10);
        assert_eq!(res.last().unwrap().0, 22551);
    }
}