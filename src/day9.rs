#[macro_use]
extern crate nom;

use nom::{alpha, digit};

static INPUT: &'static str = include_str!("../inputs/day9.txt");

// nom is goddamn magical
named!(parse_decompress<&str, u64>,
    fold_many0!(
        alt!(
            parse_decompress_alpha
            | parse_decompress_paren
        ),
        0,
        |acc, s| acc + s
    )
);
named!(parse_decompress_alpha<&str, u64>,
    map!(
        alpha,
        |s: &str| s.len() as u64
    )
);
named!(parse_decompress_paren<&str, u64>,
    do_parse!(
        tag_s!("(")  >>
        amt: num     >>
        tag_s!("x")  >>
        cnt: num     >>
        tag_s!(")")  >>
        take_s!(amt) >>
        (cnt * amt)
    )
);
named!(num<&str, u64>,
    map!(digit, |s: &str| s.parse::<u64>().unwrap())
);

named!(parse_decompress_recursive<&str, u64>,
    fold_many0!(
        alt!(
            parse_decompress_alpha
            | parse_decompress_paren_recursive
        ),
        0,
        |acc, s| acc + s
    )
);
named!(parse_decompress_paren_recursive<&str, u64>,
    do_parse!(
        tag_s!("(")                            >>
        amt: num                               >>
        tag_s!("x")                            >>
        cnt: num                               >>
        tag_s!(")")                            >>
        rest: take_s!(amt)                     >>
        (decompressed_length_recursive(rest) * cnt)
    )
);

fn decompressed_length(text: &str) -> u64 {
    parse_decompress(text).to_result().unwrap()
}
fn decompressed_length_recursive(text: &str) -> u64 {
    parse_decompress_recursive(text).to_result().unwrap()
}

fn main() {
    println!("First: {}", decompressed_length(INPUT));
    println!("Second: {}", decompressed_length_recursive(INPUT));
}

#[cfg(test)]
mod tests {
    use super::{decompressed_length, decompressed_length_recursive};
    
    #[test]
    fn part1_examples() {
        assert_eq!(decompressed_length("ADVENT"), 6);
        assert_eq!(decompressed_length("A(1x5)BC"), 7);
        assert_eq!(decompressed_length("(3x3)XYZ"), 9);
        assert_eq!(decompressed_length("A(2x2)BCD(2x2)EFG"), 11);
        assert_eq!(decompressed_length("(6x1)(1x3)A"), 6);
        assert_eq!(decompressed_length("X(8x2)(3x3)ABCY"), 18);
    }
    
    #[test]
    fn part2_examples() {
        assert_eq!(decompressed_length_recursive("(3x3)XYZ"), 9);
        assert_eq!(decompressed_length_recursive("X(8x2)(3x3)ABCY"), 20);
        assert_eq!(decompressed_length_recursive(
            "(27x12)(20x12)(13x14)(7x10)(1x12)A"), 241920);
        assert_eq!(decompressed_length_recursive(
            "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"), 445);
    }
}