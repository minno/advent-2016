static INPUT: &'static str = include_str!("../inputs/day2.txt");

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Left,
    Down,
    Right
}

struct Instructions<'a> {
    inner: std::str::Lines<'a>
}

impl<'a> Instructions<'a> {
    fn new(text: &'a str) -> Self {
        Instructions { inner: text.trim().lines() }
    }
}

impl<'a> Iterator for Instructions<'a> {
    type Item = InstructionLine<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(InstructionLine::new)
    }
}

struct InstructionLine<'a> {
    inner: std::str::Chars<'a>
}

impl<'a> InstructionLine<'a> {
    fn new(text: &'a str) -> Self {
        InstructionLine { inner: text.trim().chars() }
    }
}

impl<'a> Iterator for InstructionLine<'a> {
    type Item = Direction;
    fn next(&mut self) -> Option<Self::Item> {
        use Direction::*;
        self.inner.next().map(|ch| match ch {
            'U' => Up,
            'D' => Down,
            'L' => Left,
            'R' => Right,
            _ => panic!("Unexpected character in input: {:?}", ch)
        })
    }
}

const PAD1: &'static [u8] = b"123456789";
const PAD2: &'static [u8] = b"  1   234 56789 ABC   D  ";

fn solve(instrs: Instructions, pad: &[u8]) -> String {
    let row_width = (pad.len() as f64).sqrt() as usize;
    assert!(row_width * row_width == pad.len());
    let mut pos = pad.iter().position(|&ch| ch == b'5').unwrap();
    let mut output = String::new();
    for line in instrs {
        for direction in line {
            use Direction::*;
            let new_pos = match direction {
                Up => pos.wrapping_sub(row_width),
                Down => pos.wrapping_add(row_width),
                Left => pos.wrapping_sub(1),
                Right => pos.wrapping_add(1)
            };
            // Prevent horizontal wrapping
            if (direction == Left || direction == Right) && 
                    pos / row_width != new_pos / row_width {
                continue;
            }
            pos = match pad.get(new_pos) {
                None | Some(&b' ') => pos,
                _ => new_pos
            };
        }
        output.push(pad[pos] as char);
    }
    output
}

#[allow(dead_code)]
fn main() {
    println!("First: {}", solve(Instructions::new(INPUT), PAD1));
    println!("Second: {}", solve(Instructions::new(INPUT), PAD2));
}

#[cfg(test)]
mod tests {
    use super::{PAD1, PAD2, solve, Instructions};
    
    const TEST_INPUT: &'static str = r#"ULL
                                        RRDDD
                                        LURDL
                                        UUUUD"#;
    
    #[test]
    fn part1_examples() {
        assert_eq!(solve(Instructions::new(TEST_INPUT), PAD1), "1985");
    }
    
    #[test]
    fn part2_examples() {
        assert_eq!(solve(Instructions::new(TEST_INPUT), PAD2), "5DB3");
    }
}