#[macro_use]
extern crate nom;

use nom::digit;
use std::collections::{BinaryHeap, HashSet};
use std::cmp::Ordering;

static INPUT: &'static str = include_str!("../inputs/day13.txt");

// Like swatting a mosquito with an atom bomb
named!(parse_input<&str, i32>,
    map!(digit, |s: &str| s.parse::<i32>().unwrap())
);

struct Maze {
    seed: i32
}

impl Maze {
    fn new(seed: i32) -> Self {
        Maze { seed: seed }
    }
    
    fn is_clear(&self, x: i32, y: i32) -> bool {
        let base = x*x + 3*x + 2*x*y + y + y*y + self.seed;
        base.count_ones() & 1 == 0
    }
    
    fn path_len(&self, start: (i32, i32), end: (i32, i32)) -> i32 {
        let mut states = BinaryHeap::new();
        let mut visited = HashSet::new();
        
        states.push(SearchState::new(start, 0, end));
        visited.insert(start);
        
        while let Some(state) = states.pop() {
            let pos = state.coords;
            if pos == end {
                return state.distance;
            }
            
            let adj = [(pos.0, pos.1 + 1),
                       (pos.0, pos.1 - 1),
                       (pos.0 + 1, pos.1),
                       (pos.0 - 1, pos.1)];
            for &adj_state in adj.iter()
                    .filter(|&&(x, y)| x >= 0 && y >= 0 && 
                                       self.is_clear(x, y) &&
                                       visited.insert((x, y))) {
                states.push(SearchState::new(adj_state, state.distance + 1, end));
            }
        }
        
        panic!("I handle failure poorly");
    }
    
    fn count_nearby(&self, start: (i32, i32), distance: i32) -> i32 {
        let mut states = Vec::new();
        let mut visited = HashSet::new();
        
        states.push(start);
        visited.insert(start);
        
        for _ in 0..distance {
            let mut new_states = Vec::new();
            
            while let Some(pos) = states.pop() {
                let adj = [(pos.0, pos.1 + 1),
                           (pos.0, pos.1 - 1),
                           (pos.0 + 1, pos.1),
                           (pos.0 - 1, pos.1)];
                for &adj_state in adj.iter()
                        .filter(|&&(x, y)| x >= 0 && y >= 0 && 
                                           self.is_clear(x, y) &&
                                           visited.insert((x, y))) {
                    new_states.push(adj_state);
                }
            }
            
            states = new_states;
        }
        
        visited.len() as i32
    }
}

#[derive(Eq, PartialEq)]
struct SearchState {
    coords: (i32, i32),
    distance: i32,
    cost: i32
}

impl Ord for SearchState {
    fn cmp(&self, other: &Self) -> Ordering {
        (other.distance + other.cost).cmp(&(self.distance + self.cost))
    }
}

impl PartialOrd for SearchState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl SearchState {
    fn new(coords: (i32, i32), distance: i32, goal: (i32, i32)) -> SearchState {
        let cost = (goal.0 - coords.0).abs() + (goal.1 - coords.1).abs();
        SearchState { coords: coords, distance: distance, cost: cost }
    }
}

fn main() {
    let seed = parse_input(INPUT).to_result().unwrap();
    let maze = Maze::new(seed);
    println!("First: {}", maze.path_len((1, 1), (31, 39)));
    println!("Second: {}", maze.count_nearby((1, 1), 50));
}

#[cfg(test)]
mod test {
    use super::{Maze};
    
    #[test]
    fn part1_example() {
        let maze = Maze::new(10);
        assert_eq!(maze.path_len((1, 1), (7, 4)), 11);
    }
    
    #[test]
    fn part2() {
        let maze = Maze::new(10);
        assert_eq!(maze.count_nearby((1, 1), 0), 1);
        assert_eq!(maze.count_nearby((1, 1), 1), 3);
        assert_eq!(maze.count_nearby((1, 1), 2), 5);
        assert_eq!(maze.count_nearby((1, 1), 3), 6);
    }
}