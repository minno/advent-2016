extern crate md5;

use std::io::Write;

static INPUT: &'static str = include_str!("../inputs/day5.txt");

struct HashIter {
    buffer: Vec<u8>,
    seed_len: usize,
    curr_salt: i32
}

impl HashIter {
    fn new(data: &str) -> Self {
        let mut buf = Vec::new();
        buf.extend(data.as_bytes());
        HashIter { buffer: buf, seed_len: data.len(), curr_salt: 0 }
    }
}

impl Iterator for HashIter {
    type Item = [u8; 16];
    fn next(&mut self) -> Option<Self::Item> {
        self.buffer.truncate(self.seed_len);
        write!(&mut self.buffer, "{}", self.curr_salt).unwrap();
        self.curr_salt += 1;
        Some(md5::compute(&self.buffer).0)
    }
}

fn nib_to_char(nib: u8) -> char {
    match nib & 0x0f {
        nib @ 0...9 => (nib + '0' as u8) as char,
        nib @ _ => (nib - 10 + 'a' as u8) as char
    }
}

fn is_interesting(hash: &[u8; 16]) -> bool {
    hash[0] == 0 && hash[1] == 0 && hash[2] & 0xf0 == 0
}

fn interesting_char(hash: &[u8; 16]) -> Option<char> {
    if is_interesting(hash) {
        Some(nib_to_char(hash[2] & 0x0f))
    } else {
        None
    }
}

fn interesting_char_pos(hash: &[u8; 16]) -> Option<(usize, char)> {
    if is_interesting(hash) {
        let pos = hash[2] & 0x0f;
        if pos < 8 {
            Some((pos as usize, nib_to_char(hash[3] >> 4)))
        } else {
            None
        }
    } else {
        None
    }
}

fn part1(seed: &str) -> String {
    HashIter::new(seed)
        .filter_map(|hash| interesting_char(&hash))
        .take(8)
        .collect::<String>()
}

fn part2(seed: &str) -> String {
    let mut chars = [None; 8];
    HashIter::new(seed)
        .filter_map(|hash| interesting_char_pos(&hash))
        .filter_map(|(pos, ch)| {
            if chars[pos].is_none() { 
                chars[pos] = Some(ch);
                Some(())
            } else {
                None
            }
        })
        .take(8)
        .last(); // Force eager evaluation
    chars.iter().filter_map(|&x| x).collect()
}

fn main() {
    println!("First: {}", part1(INPUT));
    println!("Second: {}", part2(INPUT));
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    
    #[test]
    fn part1_example() {
        assert_eq!(part1("abc"), "18f47a30");
    }
    
    #[test]
    fn part2_example() {
        assert_eq!(part2("abc"), "05ace8e3");
    }
}