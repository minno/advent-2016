#[macro_use]
extern crate nom;

use nom::{alpha};
use std::collections::{BinaryHeap, HashSet};

static INPUT: &'static str = include_str!("../inputs/day11.txt");

#[derive(Clone, Default, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct State<'a> {
    elevator_pos: usize,
    floors: Vec<Vec<Component<'a>>>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Component<'a> {
    Chip(&'a str),
    Generator(&'a str),
}

named!(parse_input<&str, State>,
    fold_many0!(
        parse_line,
        State::default(),
        |state, line| add(state, line)
    )
);
fn add<'a>(mut state: State<'a>, line: Vec<Component<'a>>) -> State<'a> {
    state.floors.push(line);
    state
}
named!(parse_line<&str, Vec<Component> >,
    do_parse!(
           tag_s!("The ")
        >> ord: alpha
        >> tag_s!(" floor contains")
        >> components: parse_components
        >> (components)
    )
);
named!(parse_components<&str, Vec<Component> >,
    alt!(do_parse!(
           tag_s!(" nothing relevant.")
        >> (Vec::new())
    ) | parse_component_list
    )
);
named!(parse_component_list<&str, Vec<Component> >,
    many1!(
        do_parse!(
               opt!(tag_s!(","))
            >> opt!(tag_s!(" and"))
            >> tag_s!(" a ")
            >> element: alpha
            >> opt!(tag_s!("-compatible"))
            >> tag_s!(" ")
            >> tp: alpha
            >> opt!(tag_s!(".\n"))
            >> (match tp {
                    "generator" => Component::Generator(element),
                    "microchip" => Component::Chip(element),
                    _ => panic!("Invalid component type")
                })
        )
    )
);

fn is_allowed(state: &State) -> bool {
    fn is_generator(c: Component) -> bool {
        match c {
            Component::Generator(..) => true,
            _ => false
        }
    }
    fn is_floor_allowed(floor: &Vec<Component>) -> bool {
        // No generators, no problem
        if floor.iter().all(|c| !is_generator(*c)) {
            return true;
        }
        
        // Otherwise, each chip must be shielded by its own generator
        floor.iter().all(|&item| {
            if let Component::Chip(element) = item {
                floor.contains(&Component::Generator(element))
            } else {
                true
            }
        })
    }
    
    fn special_rule_fails(state: &State) -> bool {
        // Special rule: don't have more than 2 chips on the top floor
        // with no generators, since that makes it impossible to add
        // the generators without removing some chips, so it (hopefully)
        // won't be required on the optimum path.
        // I noticed my search was wasting a lot of time on those states
        let top_floor = state.floors.last().unwrap();
        top_floor.len() > 2 && top_floor.iter().all(|el| !is_generator(*el))
    }
    
    state.floors.iter().all(is_floor_allowed) && !special_rule_fails(state)
}

fn is_goal(state: &State) -> bool {
    state.floors[..3].iter().all(|vec| vec.is_empty())
}

fn canonicalize(mut state: State) -> State {
    for floor in &mut state.floors {
        floor.sort();
    }
    state
}

fn cost(state: &State) -> i32 {
    let mut cost = 0;
    let mut component_counts = state.floors.iter()
        .map(|floor| floor.len() as i32).collect::<Vec<_>>();
    let mut lowest_floor = component_counts.iter()
        .position(|ct| *ct > 0).unwrap();
    cost += (state.elevator_pos as i32 - lowest_floor as i32).abs();
    while lowest_floor < component_counts.len() - 1 {
        let num_trips = (component_counts[lowest_floor] - 1) * 2 - 1;
        cost += std::cmp::max(num_trips, 1);
        component_counts[lowest_floor+1] += component_counts[lowest_floor];
        lowest_floor += 1;
    }
    cost
}

fn moves<'a>(state: &State<'a>) -> Vec<State<'a>> {
    // Choose any 1 or 2 items and move up or down one floor
    let mut states = Vec::new();
    let items = &state.floors[state.elevator_pos];
    let floor = state.elevator_pos;
    let dest_floors = &[floor.wrapping_sub(1), floor + 1][
        if floor == 0 {
            1..2
        } else if floor == state.floors.len() - 1 {
            0..1
        } else {
            0..2
        }
    ];
    
    for first_move in 0..items.len() {
        for second_move in first_move..items.len() {
            for &new_floor in dest_floors.iter() {
                let mut new_state = state.clone();
                new_state.elevator_pos = new_floor;
                let first_item = new_state.floors[floor].swap_remove(first_move);
                new_state.floors[new_floor].push(first_item);
                if second_move != items.len()-1 {
                    let second_item = new_state.floors[floor].swap_remove(second_move);
                    new_state.floors[new_floor].push(second_item);
                }
                if is_allowed(&new_state) {
                    states.push(canonicalize(new_state));
                }
            }
        }
    }
    
    states.sort();
    states.dedup();
    states
}

#[derive(PartialEq, Eq, Debug)]
struct SearchState<'a> {
    num_steps: i32,
    cost: i32,
    state: State<'a>    
}

impl<'a> SearchState<'a> {
    fn new(state: State<'a>, num_steps: i32) -> Self {
        SearchState {
            num_steps: num_steps,
            cost: cost(&state),
            state: state
        }
    }
}

impl<'a> Ord for SearchState<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        (other.num_steps + other.cost).cmp(&(self.num_steps + self.cost))
    }
}

impl<'a> PartialOrd for SearchState<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

// If it found a goal state, returns the number of steps it took to get there
fn step<'a>(states: &mut BinaryHeap<SearchState<'a>>, 
            past_states: &mut HashSet<State<'a>>) -> Option<i32> {
    let curr = states.pop().unwrap();
    let new_states = moves(&curr.state);
    if new_states.iter().any(is_goal) {
        return Some(curr.num_steps + 1);
    }
    for state in new_states.into_iter() {
        if past_states.insert(canonicalize(state.clone())) {
            states.push(SearchState::new(state, curr.num_steps + 1));
        }
    }
    None
}

fn num_steps_to_goal(start: &State) -> i32 {
    let mut states = BinaryHeap::new();
    let mut past_states = HashSet::new();
    states.push(SearchState::new(start.clone(), 0));
    loop {
        if let Some(num_steps) = step(&mut states, &mut past_states) {
            return num_steps;
        }
    }
}

fn main() {
    let mut state = parse_input(INPUT).to_result().unwrap();
    println!("First: {}", num_steps_to_goal(&state));
    
    state.floors[0].extend(&[
        Component::Generator("elerium"),
        Component::Chip("elerium"),
        Component::Generator("dilithium"),
        Component::Chip("dilithium")
    ][..]);
    println!("Second: {}", num_steps_to_goal(&state));
}