
static INPUT: &'static str = include_str!("../inputs/day3.txt");

fn is_triangle((x, y, z): (i32, i32, i32)) -> bool {
    (x + y) > z && (x + z) > y && (y + z) > x
}

struct Triangles<'a> {
    inner: std::str::SplitWhitespace<'a>,
}

impl<'a> Triangles<'a> {
    fn new(input: &'a str) -> Self {
        Triangles { inner: input.split_whitespace() }
    }
}

impl<'a> Iterator for Triangles<'a> {
    type Item = (i32, i32, i32);
    fn next(&mut self) -> Option<Self::Item> {
        let mut nums = (&mut self.inner).map(|n| n.parse::<i32>().unwrap());
        if let (Some(a), Some(b), Some(c)) = 
                (nums.next(), nums.next(), nums.next()) {
            Some((a, b, c))
        } else {
            None
        }
    }
}

struct ColTriangles<'a> {
    inner: std::str::Lines<'a>,
    buffer: [i32; 9],
    curr_idx: usize,
}

impl<'a> ColTriangles<'a> {
    fn new(input: &'a str) -> Self {
        ColTriangles {
            inner: input.lines(),
            buffer: [0; 9],
            curr_idx: 3
        }
    }
}

impl<'a> Iterator for ColTriangles<'a> {
    type Item = (i32, i32, i32);
    fn next(&mut self) -> Option<Self::Item> {
        if self.curr_idx >= 3 {
            let mut buffer_idx = 0;
            // Read 3 lines to fill buffer
            for _ in 0..3 {
                let line = match self.inner.next() {
                    Some(line) => line,
                    None => return None
                };
                for num in line.split_whitespace()
                        .map(|n| n.parse::<i32>().unwrap()) {
                    self.buffer[buffer_idx] = num;
                    buffer_idx += 1;
                }
            }
            self.curr_idx = 0;
        }
        
        let col = self.curr_idx;
        self.curr_idx += 1;
        Some((self.buffer[col], self.buffer[col+3], self.buffer[col+6]))
    }
}

fn main() {
    let first = Triangles::new(INPUT).filter(|tup| is_triangle(*tup)).count();
    let second = ColTriangles::new(INPUT).filter(|tup| is_triangle(*tup)).count();
    println!("First: {}", first);
    println!("Second: {}", second);
}

#[cfg(test)]
mod tests {
    // No test cases given
}