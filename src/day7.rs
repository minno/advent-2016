#[macro_use]
extern crate nom;

use nom::alpha;

mod utils;
use utils::IteratorExt;

static INPUT: &'static str = include_str!("../inputs/day7.txt");

struct Address<'a> {
    outside: Vec<&'a str>,
    inside: Vec<&'a str>
}

// Nom parsers
named!(parse_bracketed<&str, &str>,
    do_parse!(
        tag_s!("[") >>
        s: alpha    >>
        tag_s!("]") >>
        (s)
    )
);
named!(parse_address<&str, Address>, 
    do_parse!(
        segments: many0!(
            alt!(parse_bracketed | alpha)
        ) >>
        ({let outside = segments.iter().cloned().step(2).collect();
          let inside = segments.iter().cloned().skip(1).step(2).collect();
          Address { outside: outside, inside: inside }})
    )
);

fn supports_tls(address: &Address) -> bool {
    fn has_abba(s: &str) -> bool {
        s.as_bytes().windows(4).any(|slice| {
            slice[0] == slice[3] && slice[1] == slice[2] && slice[0] != slice[1]
        })
    }
    
    address.outside.iter().cloned().any(has_abba) && 
        !address.inside.iter().cloned().any(has_abba)
}

fn supports_ssl(address: &Address) -> bool {
    fn is_aba(s: &[u8]) -> bool {
        s[0] == s[2] && s[0] != s[1]
    }
    
    fn has_bab(s: &str, aba: &[u8]) -> bool {
        let bab = &[aba[1], aba[0], aba[1]][..];
        s.as_bytes().windows(3).any(|slice| slice == bab)
    }
    
    for segment in &address.outside {
        for aba in segment.as_bytes().windows(3).filter(|piece| is_aba(*piece)) {
            if address.inside.iter().any(|inner_segment| has_bab(inner_segment, aba)) {
                return true
            }
        }
    }
    false
}

fn main() {
    let (first, second) = INPUT.lines()
        .filter_map(|line| parse_address(line).to_result().ok())
        .fold((0, 0), |(mut tls_count, mut ssl_count), address| {
            if supports_tls(&address) {
                tls_count += 1;
            }
            if supports_ssl(&address) {
                ssl_count += 1;
            }
            (tls_count, ssl_count)
        });
    println!("First: {}", first);
    println!("Second: {}", second);
}

#[cfg(test)]
mod tests {
    use super::{parse_address, supports_tls, supports_ssl};
    
    #[test]
    fn part1_examples() {
        let inputs = ["abba[mnop]qrst",
                      "abcd[bddb]xyyx",
                      "aaaa[qwer]tyui",
                      "ioxxoj[asdfgh]zxcvbn"];
        let answers = [true, false, false, true];
        for (&input, &answer) in inputs.iter().zip(answers.iter()) {
            println!("Testing {}", input);
            let address = parse_address(input).to_result().unwrap();
            assert_eq!(supports_tls(&address), answer);
        }
    }
    
    #[test]
    fn part2_examples() {
        let inputs = ["aba[bab]xyz",
                      "xyx[xyx]xyx",
                      "aaa[kek]eke",
                      "zazbz[bzb]cdb"];
        let answers = [true, false, true, true];
        for (&input, &answer) in inputs.iter().zip(answers.iter()) {
            println!("Testing {}", input);
            let address = parse_address(input).to_result().unwrap();
            assert_eq!(supports_ssl(&address), answer);
        }
    }
}