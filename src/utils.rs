#![allow(dead_code)]

use std::fs::File;
use std::io::{Read, BufReader, BufRead};
use std::iter::FromIterator;

fn open_input(day: i32) -> File {
    File::open(format!("./inputs/day{}.txt", day)).unwrap()
}

pub fn get_input(day: i32) -> String {
    let mut f = open_input(day);
    let mut buf = String::new();
    f.read_to_string(&mut buf).unwrap();
    buf
}

pub fn get_lines<F, T, C>(day: i32, mut parser: F) -> C
        where F: FnMut(String) -> T, C: FromIterator<T> {
    let reader = BufReader::new(open_input(day));
    reader.lines().filter_map(|el| el.ok().map(|s| parser(s))).collect()
}

// Fancy iterator shit
pub struct Step<I: Iterator> {
    inner: I,
    step: usize
}

impl<I: Iterator> Iterator for Step<I> {
    type Item = I::Item;
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.inner.next();
        for _ in 0..self.step-1 {
            self.inner.next();
        }
        next
    }
}

pub trait IteratorExt<T>: Sized + Iterator<Item=T> {
    fn step(self, n: usize) -> Step<Self> {
        Step { inner: self, step: n }
    }
}

impl<I> IteratorExt<I::Item> for I where I: Iterator {}

