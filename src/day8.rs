#[macro_use]
extern crate nom;

use nom::digit;

mod utils;
use utils::IteratorExt;

static INPUT: &'static str = include_str!("../inputs/day8.txt");

#[derive(Copy, Clone, Debug)]
enum Command {
    Rect {
        width: usize,
        height: usize
    },
    RotateRow {
        row: usize,
        amount: usize
    },
    RotateColumn {
        col: usize,
        amount: usize
    }
}

// Nom parsers
named!(parse_command<&str, Command>,
    alt!(parse_rect | parse_rotate_row | parse_rotate_col)
);
named!(parse_rect<&str, Command>,
    do_parse!(
        tag_s!("rect ") >>
        w: digit        >>
        tag_s!("x")     >>
        h: digit        >>
        (Command::Rect{ width: w.parse().unwrap(), height: h.parse().unwrap() })
    )
);
named!(parse_rotate_row<&str, Command>,
    do_parse!(
        tag_s!("rotate row y=") >>
        row: digit              >>
        tag_s!(" by ")          >>
        amt: digit              >>
        (Command::RotateRow{ row: row.parse().unwrap(), amount: amt.parse().unwrap() })
    )
);
named!(parse_rotate_col<&str, Command>,
    do_parse!(
        tag_s!("rotate column x=") >>
        col: digit                 >>
        tag_s!(" by ")             >>
        amt: digit                 >>
        (Command::RotateColumn{ col: col.parse().unwrap(), amount: amt.parse().unwrap() })
    )
);

#[derive(Clone)]
struct Board {
    pixels: Vec<bool>,
    width: usize,
    height: usize,
}

impl Board {
    fn new(width: usize, height: usize) -> Board {
        Board { pixels: vec![false; width * height], width: width, height: height }
    }

    fn act(&mut self, command: Command) {
        use Command::*;
        match command {
            Rect { width, height } => {
                for row in 0..height {
                    for offset in 0..width {
                        self.pixels[row * self.width + offset] = true;
                    }
                }
            }
            RotateRow { row, amount } => {
                let start = row * self.width;
                let breakpoint = start + self.width - amount;
                let row_pixels = self.pixels[breakpoint..start+self.width]
                    .iter().cloned().chain(
                        self.pixels[start..breakpoint]
                        .iter().cloned())
                    .collect::<Vec<_>>();
                for i in 0..self.width {
                    self.pixels[i+start] = row_pixels[i];
                }
            },
            RotateColumn { col, amount } => {
                let start = col;
                let rev_amount = self.height - amount;
                let col_pixels = self.pixels[start..]
                    .iter().cloned().step(self.width).skip(rev_amount).chain(
                        self.pixels[start..]
                        .iter().cloned().step(self.width).take(rev_amount)
                    ).collect::<Vec<_>>();
                for i in 0..self.height {
                    self.pixels[start + i*self.width] = col_pixels[i];
                }
            }
        }
    }
}

impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        for (idx, &pixel) in self.pixels.iter().enumerate() {
            write!(f, "{}", if pixel { '#' } else { '.' })?;
            if idx % self.width == self.width - 1 {
                write!(f, "{}", '\n')?;
            }
        }
        Ok(())
    }
}

struct Commands<'a> {
    inner: std::str::Lines<'a>
}

impl<'a> Commands<'a> {
    fn new(s: &'a str) -> Self {
        Commands { inner: s.lines() }
    }
}

impl<'a> Iterator for Commands<'a> {
    type Item = Command;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().and_then(|line| parse_command(line).to_result().ok())
    }
}

fn apply_all(board: &mut Board, commands: Commands) {
    for command in commands {
        board.act(command);
    }
}

fn main() {
    let mut board = Board::new(50, 6);
    apply_all(&mut board, Commands::new(INPUT));
    println!("Number lit: {}", board.pixels.iter().filter(|&&x| x).count());
    println!("{}", board);
}

#[cfg(test)]
mod test {
    use super::{Board, Commands, apply_all};
    
    static COMMANDS: &'static str = r#"rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1"#;
    
    #[test]
    fn part1_example() {
        let mut board = Board::new(7, 3);
        apply_all(&mut board, Commands::new(COMMANDS));
        assert_eq!(format!("{}", board).trim(), ".#..#.#\n#.#....\n.#.....");
    }
}