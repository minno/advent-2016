use std::collections::HashSet;

static INPUT: &'static str = include_str!("../inputs/day1.txt");

#[derive(Copy, Clone, Debug)]
enum Action {
    Left(i32),
    Right(i32)
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Left,
    Down,
    Right
}

impl Direction {
    fn left(self) -> Direction {
        use Direction::*;
        match self {
            Up => Left,
            Left => Down,
            Down => Right,
            Right => Up
        }
    }
    
    fn right(self) -> Direction {
        self.left().left().left()
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32
}

impl Point {
    fn distance(self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

#[derive(Copy, Clone, Debug)]
struct State {
    dir: Direction,
    pos: Point,
}

impl Default for State {
    fn default() -> Self {
        State { 
            dir: Direction::Up, 
            pos: Point { x: 0, y: 0 } 
        }
    }
}

struct Actions<'a> {
    inner: std::str::SplitTerminator<'a, &'static str>,
}

impl<'a> Actions<'a> {
    fn new(input: &'a str) -> Actions<'a> {
        Actions { inner: input.split_terminator(", ") }
    }
}

impl<'a> Iterator for Actions<'a> {
    type Item = Action;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|dir| {
            let distance = dir[1..].parse().unwrap();
            if dir.chars().nth(0) == Some('L') {
                Action::Left(distance)
            } else {
                Action::Right(distance)
            }
        })
    }
}

fn apply(state: State, action: Action) -> State {
    use Direction::*;
    let (new_dir, distance) = match action {
        Action::Left(distance) => (state.dir.left(), distance),
        Action::Right(distance) => (state.dir.right(), distance)
    };
    
    let new_pos = match new_dir {
        Up => Point { x: state.pos.x, y: state.pos.y + distance },
        Down => Point { x: state.pos.x, y: state.pos.y - distance },
        Left => Point { x: state.pos.x - distance, y: state.pos.y },
        Right => Point { x: state.pos.x + distance, y: state.pos.y },
    };
    
    State { dir: new_dir, pos: new_pos }
}

fn first_visited_twice(positions: &[Point]) -> Option<Point> {
    let mut all_visited = HashSet::new();
    for (&p1, &p2) in positions.iter().zip(positions.iter().skip(1)) {
        let is_horizontal = p1.y == p2.y;
        let is_positive = if is_horizontal {
            p2.x > p1.x
        } else {
            p2.y > p1.y
        };
        let distance = (p2.x - p1.x).abs() + (p2.y - p1.y).abs();
        for i in 0..distance {
            let p = match (is_horizontal, is_positive) {
                (true, true) => Point{ x: p1.x + i, y: p1.y },
                (true, false) => Point{ x: p1.x - i, y: p1.y },
                (false, true) => Point{ x: p1.x, y: p1.y + i },
                (false, false) => Point{ x: p1.x, y: p1.y - i },
            };
            if !all_visited.insert(p) {
                return Some(p);
            }
        }
    }
    None
}

fn solve(actions: &mut Actions) -> (i32, Option<i32>) {
    let mut state = State::default();
    let mut positions = vec![state.pos];
    for act in actions {
        state = apply(state, act);
        positions.push(state.pos);
    }
    (state.pos.distance(), first_visited_twice(&positions).map(Point::distance))
}

fn main() {
    let (ans1, ans2) = solve(&mut Actions::new(INPUT));
    println!("Distance to final position: {}", ans1);
    println!("Distance to first point visited twice: {}", ans2.unwrap());
}

#[cfg(test)]
mod tests {
    use super::{solve, Actions};
    
    #[test]
    fn part1_examples() {
        assert_eq!(solve(&mut Actions::new("R2, L3")).0, 5);
        assert_eq!(solve(&mut Actions::new("R2, R2, R2")).0, 2);
        assert_eq!(solve(&mut Actions::new("R5, L5, R5, R3")).0, 12);
    }
    
    #[test]
    fn part2_examples() {
        assert_eq!(solve(&mut Actions::new("R8, R4, R4, R8")).1, Some(4))
    }
}